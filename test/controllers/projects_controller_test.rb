require 'test_helper'

class ProjectsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @project = projects(:project1)
    @institution = institutions(:institution1)
    @user = users(:user1)
    @project.institution_id = @institution.id
    @project.user_id = @user.id
    @project.save

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:projects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create project" do
    assert_difference('Project.count') do
      post :create, project: { name: "Project1", institution_id: @institution.id, user_id: @user.id }
    end

    assert_redirected_to project_path(assigns(:project))
  end

  test "should not create project" do
    assert_no_difference('Project.count') do
      post :create, project: { name: "", institution_id: @institution.id, user_id: @user.id }
    end

    assert_equal(2, Project.count)
  end

  test "should show project" do
    get :show, id: @project
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @project
    assert_response :success
  end

  test "should update project" do
    patch :update, id: @project, project: { name: "Project4" }
    assert_redirected_to project_path(assigns(:project))
  end

  test "should not update project" do
    patch :update, id: @project, project: { name: "" }
    assert_template :edit
  end

  test "should destroy project" do
    assert_difference('Project.count', -1) do
      delete :destroy, id: @project
    end

    assert_redirected_to projects_path
  end
end
