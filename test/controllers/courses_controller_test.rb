require 'test_helper'

class CoursesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @course = courses(:course1)
    @user = users(:user1)
    @institution = institutions(:institution1)
    @course.institution_id = @institution.id
    @course.user_id = @user.id
    @course.save

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create course" do
    assert_difference('Course.count') do
      post :create, course: { name: "Testing course", institution_id: @institution.id }
    end

    assert_redirected_to course_path(assigns(:course))
  end

  test "should not create course" do
    assert_no_difference('Course.count') do
      post :create, course: { name: "", institution_id: nil }
    end

    assert_equal(2, Course.count)
  end

  test "should show course" do
    get :show, id: @course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @course
    assert_response :success
  end

  test "should update course" do
    patch :update, id: @course, course: { name: "Testing course", institution_id: @institution.id }
    assert_redirected_to course_path(assigns(:course))
  end

  test "should not update course" do
    patch :update, id: @course, course: { name: "", institution_id: @institution.id }
    assert_template :edit
  end

  test "should destroy course" do
    assert_difference('Course.count', -1) do
      delete :destroy, id: @course
    end

    assert_redirected_to courses_path
  end
end
