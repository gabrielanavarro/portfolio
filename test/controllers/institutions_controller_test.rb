require 'test_helper'

class InstitutionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @institution = institutions(:institution1)
    @user = users(:user1)
    @address = addresses(:address1)
    @institution.addresses_id = @address.id
    @institution.save

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:institutions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create institution" do
    assert_difference('Institution.count') do
      post :create, institution: { name: "Testing Service", description: "This is a description for a testing service", acronym: "TS", addresses_id: @address.id  }
    end

    assert_redirected_to institution_path(assigns(:institution))
  end

  test "should not create institution" do
    assert_no_difference('Institution.count') do
      post :create, institution: { name: "", description: "This is a description for a testing service", acronym: "TS", addresses_id: @address.id  }
    end

    assert_equal(2, Institution.count)
  end

  test "should show institution" do
    get :show, id: @institution
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @institution
    assert_response :success
  end

  test "should update institution" do
    patch :update, id: @institution, institution: { name: "Testing Service" }
    assert_redirected_to institution_path(assigns(:institution))
  end

  test "should not update institution" do
    patch :update, id: @institution, institution: { name: "" }
    assert_template :edit
  end

  test "should destroy institution" do
    assert_difference('Institution.count', -1) do
      delete :destroy, id: @institution
    end

    assert_redirected_to institutions_path
  end
end
