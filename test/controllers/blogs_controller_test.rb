require 'test_helper'

class BlogsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @blog = blogs(:blog1)
    @user = users(:user1)
    @blog.user_id = @user.id
    @blog.save

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:blogs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create blog" do
    assert_difference('Blog.count') do
      post :create, blog: { name: "Blog123", description: "Blog description for the test", user_id: @user.id }
    end

    assert_redirected_to blog_path(assigns(:blog))
  end

  test "should not create blog" do
    assert_no_difference('Blog.count') do
      post :create, blog: { name: "", description: "Blog description for the test", user_id: @user.id }
    end

    assert_equal(2, Blog.count)
  end

  test "should show blog" do
    get :show, id: @blog
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @blog
    assert_response :success
  end

  test "should update blog" do
    patch :update, id: @blog, blog: { name: "Updated Blog", user_id: @user.id }
    assert_redirected_to blog_path(assigns(:blog))
  end

  test "should not update blog" do
    patch :update, id: @blog, blog: { name: "", user_id: @user.id }
    assert_template :edit
  end

  test "should destroy blog" do
    assert_difference('Blog.count', -1) do
      delete :destroy, id: @blog
    end

    assert_redirected_to blogs_path
  end
end
