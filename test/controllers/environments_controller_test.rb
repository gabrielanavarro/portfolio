require 'test_helper'

class EnvironmentsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @environment = environments(:environment1)
    @user = users(:user1)

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:environments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create environment" do
    assert_difference('Environment.count') do
      post :create, environment: { name: "Portfolio4" }
    end

    assert_redirected_to environment_path(assigns(:environment))
  end

  test "should not create environment" do
    assert_no_difference('Environment.count') do
      post :create, environment: { name: "" }
    end

    assert_equal(2, Environment.count)
  end

  test "should show environment" do
    get :show, id: @environment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @environment
    assert_response :success
  end

  test "should update environment" do
    patch :update, id: @environment, environment: { name: "Portfolio3" }
    assert_redirected_to environment_path(assigns(:environment))
  end

  test "should not update environment" do
    patch :update, id: @environment, environment: { name: "" }
    assert_template :edit
  end

  test "should destroy environment" do
    assert_difference('Environment.count', -1) do
      delete :destroy, id: @environment
    end

    assert_redirected_to environments_path
  end
end
