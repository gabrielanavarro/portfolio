require 'test_helper'

class JobsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @job = jobs(:job1)
    @institution = institutions(:institution1)
    @user = users(:user1)
    @job.institution_id = @institution.id
    @job.user_id = @user.id
    @job.save

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:jobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create job" do
    assert_difference('Job.count') do
      post :create, job: { name: "Developer Junior", institution_id: @institution.id, user_id: @user.id }
    end

    assert_redirected_to job_path(assigns(:job))
  end

  test "should not create job" do
    assert_no_difference('Job.count') do
      post :create, job: { name: "", institution_id: @institution.id, user_id: @user.id }
    end

    assert_equal(2, Job.count)
  end

  test "should show job" do
    get :show, id: @job
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @job
    assert_response :success
  end

  test "should update job" do
    patch :update, id: @job, job: { name: "Developer2" }
    assert_redirected_to job_path(assigns(:job))
  end

  test "should not update job" do
    patch :update, id: @job, job: { name: "" }
    assert_template :edit
  end

  test "should destroy job" do
    assert_difference('Job.count', -1) do
      delete :destroy, id: @job
    end

    assert_redirected_to jobs_path
  end
end
