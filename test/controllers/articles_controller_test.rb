require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @article = articles(:article1)
    @user = users(:user1)
    @blog = blogs(:blog1)
    @article.user_id = @user.id
    @article.blog_id = @blog.id
    @article.save

    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end

  test "render access denied if user is not login" do
    sign_out @user
    get :new
    assert_response 302
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create article" do
    assert_difference('Article.count') do
      post :create, article: { title: "Article 1", abstract: "This is an abstract", body: "This is a body", blog_id: @blog.id }
    end

    assert_redirected_to article_path(assigns(:article))
  end

  test "should not create article" do
    post :create, article: { title: "Article 1", body: "This is a body", blog_id: @blog.id }

    assert_equal(2, Article.count)
  end

  test "should show article" do
    get :show, id: @article
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @article
    assert_response :success
  end

  test "should update article" do
    patch :update, id: @article, article: { title: "Updated", blog_id: @blog.id }
    assert_redirected_to article_path(assigns(:article))
  end

  test "should not update article" do
    patch :update, id: @article, article: { title: "Updated", blog_id: @blog.id, abstract: "" }
    assert_template :edit
  end

  test "should destroy article" do
    assert_difference('Article.count', -1) do
      delete :destroy, id: @article
    end

    assert_redirected_to articles_path
  end
end
