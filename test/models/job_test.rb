require 'test_helper'

class JobTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.create(email: "test123@test.com", password: "12345678", password_confirmation: "12345678")
    @inst1 = Institution.create(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnT")
  end

  def teardown
    User.destroy_all
    Institution.destroy_all
    Job.destroy_all
  end

  test "not save if name is not present" do
    job = Job.new(institution_id: @inst1.id, user_id: @user.id)

    assert_not job.save, "Saved with no name"
  end

  test "not save if institution is not present" do
    job = Job.new(name: "Student", user_id: @user.id)

    assert_not job.save, "Saved with no institution"
  end

  test "not save if user is not present" do
    job = Job.new(institution_id: @inst1.id, name: "Student")

    assert_not job.save, "Saved with no user"
  end

  test "not save if name is already taken" do
    job1 = Job.create(name: "Student", institution_id: @inst1.id, user_id: @user.id)
    job2 = Job.new(name: "Student", institution_id: @inst1.id, user_id: @user.id)

    assert_not job2.save, "Saved with same name for the same user"
  end
end
