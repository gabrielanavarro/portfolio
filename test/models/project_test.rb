require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.create(email: "test123@test.com", password: "12345678", password_confirmation: "12345678")
    @inst1 = Institution.create(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnT")
  end

  def teardown
    User.destroy_all
    Institution.destroy_all
    Project.destroy_all
  end

  test "not save if name is not present" do
    project = Project.new(institution_id: @inst1.id, user_id: @user.id)

    assert_not project.save, "Saved with no name"
  end

  test "not save if institution is not present" do
    project = Project.new(name: "Student", user_id: @user.id)

    assert_not project.save, "Saved with no institution"
  end

  test "not save if user is not present" do
    project = Project.new(institution_id: @inst1.id, name: "Student")

    assert_not project.save, "Saved with no user"
  end

  test "not save if name is already taken" do
    project1 = Project.create(name: "Student", institution_id: @inst1.id, user_id: @user.id)
    project2 = Project.new(name: "Student", institution_id: @inst1.id, user_id: @user.id)

    assert_not project2.save, "Saved with same name for the same user"
  end
end
