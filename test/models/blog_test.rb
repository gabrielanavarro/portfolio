require 'test_helper'

class BlogTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.create(email: "user@user.com", password: "12345678", password_confirmation: "12345678")
  end

  def teardown
    User.destroy_all
    Blog.destroy_all
  end

  test "cant save blog without name" do
    blog = Blog.new(description: "This is a blog to prove that the validations are working", user_id: @user.id)

    assert_not blog.save, "Saved blog without name"
  end

  test "cant save blog without description" do
    blog = Blog.new(name: "Blog of testing", user_id: @user.id)

    assert_not blog.save, "Saved blog without description"
  end

  test "cant save blog without user" do
    blog = Blog.new(name: "Blog of testing", description: "This is a blog to prove that the validations are working")

    assert_not blog.save, "Saved blog without user"
  end

  test "cant save blog when there is already one blog with that name" do
    blog1 = Blog.create(name: "Blog of testing", description: "This is a blog to prove that the validations are working", user_id: @user.id)
    blog2 = Blog.new(name: "Blog of testing", description: "This is a blog to prove that the validations are working", user_id: @user.id)

    assert_not blog2.save, "Saved blog with duplicate name"
  end
end
