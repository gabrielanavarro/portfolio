require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.create(email: "user@user.com", password: "12345678", password_confirmation: "12345678")
    @institution = Institution.create(name: "University of testing", description: "At this university you basically learns how to test", acronym: "UnT")
  end

  def teardown
    User.destroy_all
    Course.destroy_all
  end


  test "not save course without name" do
    course = Course.new(institution_id: @institution.id, user_id: @user.id)

    assert_not course.save, "Saved without name"
  end

  test "not save course without institution" do
    course = Course.new(name: "Testing 101", user_id: @user.id)

    assert_not course.save, "Saved without institution"
  end

  test "not save course without user" do
    course = Course.new(institution_id: @institution.id, name: "Testing 102")

    assert_not course.save, "Saved without user"
  end
end
