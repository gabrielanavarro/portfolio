require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "not save address without identifier" do
    address = Address.new(country: "Brazil", state: "DF", city: "Brasilia")

    assert_not address.save, "Saved without idenfitifer"
  end


  test "not save address with same identifier" do
    address1 = Address.create(country: "Brazil", state: "DF", city: "Brasilia", identifier: "Brasilia - DF")
    address2 = Address.new(country: "Brazil", state: "DF", city: "Brasilia", identifier: "Brasilia - DF")

    assert_not address2.save, "Saved with same identifier"
  end


  test "not save address without country" do
    address = Address.new(state: "DF", city: "Brasilia", identifier: "Brasilia - DF")

    assert_not address.save, "Saved without country"
  end


  test "not save address without city" do
    address = Address.new(country: "Brazil", state: "DF", identifier: "Brasilia - DF")

    assert_not address.save, "Saved without city"
  end


  test "not save address without state" do
    address = Address.new(state: "DF", country: "Brazil", identifier: "Brasilia - DF")

    assert_not address.save, "Saved without state"
  end

end
