require 'test_helper'

class EnvironmentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def teardown
    Environment.destroy_all
  end

  test "not save course without name" do
    environment = Environment.new()

    assert_not environment.save, "Saved without name"
  end

  test "not save course with same name but with case sensitive" do
    environment1 = Environment.create(name: "Testing")
    environment2 = Environment.new(name: "testing")

    assert_not environment2.save, "Saved with same name but case sensitive"
  end

  test "not save course with same name" do
    environment1 = Environment.create(name: "Testing")
    environment2 = Environment.new(name: "Testing")

    assert_not environment2.save, "Saved with same name"
  end



end
