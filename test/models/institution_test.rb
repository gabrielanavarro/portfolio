require 'test_helper'

class InstitutionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def teardown
    Institution.destroy_all
  end

  test "not create institution without name" do
    inst = Institution.new(description: "This institution is to teach people how to test", acronym: "UnT")

    assert_not inst.save, "Saved without name"
  end

  test "not create institution with same name" do
    inst1 = Institution.create(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnT")
    inst2 = Institution.new(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnOT")

    assert_not inst2.save, "Saved with same name"
  end

  test "not create institution with same name case sensitive" do
    inst1 = Institution.create(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnT")
    inst2 = Institution.new(name: "university of testing", description: "This institution is to teach people how to test", acronym: "UnOT")

    assert_not inst2.save, "Saved with same name case sensitive"
  end

  test "not create institution without description" do
    inst = Institution.new(name: "University of Testing", acronym: "UnT")

    assert_not inst.save, "Saved without description"
  end

  test "not create institution without acronym" do
    inst = Institution.new(description: "This institution is to teach people how to test", name: "University of Testing")

    assert_not inst.save, "Saved without acronym"
  end

  test "not create institution with same acronym" do
    inst1 = Institution.create(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnT")
    inst2 = Institution.new(name: "University of Tests", description: "This institution is to teach people how to test", acronym: "UnT")

    assert_not inst2.save, "Saved with same acronym"
  end

  test "not create institution with same acronym case sensitive" do
    inst1 = Institution.create(name: "University of Testing", description: "This institution is to teach people how to test", acronym: "UnT")
    inst2 = Institution.new(name: "University of Tests", description: "This institution is to teach people how to test", acronym: "unt")

    assert_not inst2.save, "Saved with same acronym case sensitive"
  end
end
