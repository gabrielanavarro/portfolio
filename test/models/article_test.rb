require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.create(email: "test123@test.com", password: "12345678", password_confirmation: "12345678")
    @blog = Blog.create(name: "The blog", user_id: @user.id)
  end

  def teardown
    User.destroy_all
    Blog.destroy_all
    Article.destroy_all
  end

  test "article cant be saved if title is not present" do
    article = Article.new(body: "Body of the article", abstract: "Abstract of the article", blog_id: @blog.id)

    assert_not article.save, "Saved the article without title"
  end

  test "article cant be saved if body is not present" do
    article = Article.new(title: "Title of the article", abstract: "Abstract of the article", blog_id: @blog.id)

    assert_not article.save, "Saved the article without body"
  end

  test "article cant be saved if abstract is not present" do
    article = Article.new(title: "Title of the article", body: "Body of the article", blog_id: @blog.id)

    assert_not article.save, "Saved the article without abstract"
  end

  test "article cant be saved if blog is not present" do
    article = Article.new(title: "Title of the article", body: "Body of the article", abstract: "Abstract of this blog")

    assert_not article.save, "Saved the article without blog"
  end

  test "article cant be saved if title is not unique within the same blog" do
    article1 = Article.create(title: "Title of the article", abstract: "Abstract of the article", blog_id: @blog.id, body: "Body of this article")
    article2 = Article.new(title: "Title of the article", abstract: "Abstract of the article", blog_id: @blog.id, body: "Body of this article")

    assert_not article2.save, "Saved the article with two articles with the same title in the same blog"
  end
end
