echo "This is the update"
sudo apt-get update
echo "Downloading Ruby"
wget http://cache.ruby-lang.org/pub/ruby/2.2/ruby-2.2.1.tar.gz
tar -zxf ruby-2.2.1.tar.gz
cd ~/portfolio/ruby-2.2.1
sudo apt-get install zlib1g-dev openssl libssl-dev libruby2.0 libreadline-dev git-core tk8.6-dev libffi-dev git software-properties-common
./configure
make
sudo make install
echo "Back to portfolio"
cd ~/portfolio/
echo pwd
rm -rf ruby-2.2.1
rm ruby-2.2.1.tar.gz
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs
sudo gem install rails -V
sudo apt-get install postgresql libpq-dev phantomjs redis-server libicu-dev cmake g++ pkg-config krb5-*
bundle install
# create the database with sample data
echo 'Configuring Portfolio to use PostgreSQL, with your user.'
if [ -z "$USER" ]; then
  USER=$(stat -c %U $0)
fi
sed -ri "s/username: postgres/username: $USER/" config/database.yml
sed -ri "s/password: postgres/password: /" config/database.yml
sudo su - postgres -c "createuser $USER --no-superuser --createdb --no-createrole" || true
sudo su - postgres -c "createdb portfolio_development -O $USER" || true
