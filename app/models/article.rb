class Article < ActiveRecord::Base

  validates :title, presence: true
  validates :abstract, presence: true
  validates :body, presence: true
  validates :blog_id, presence: true

  validate :check_if_title_is_unique_on_blog

  def check_if_title_is_unique_on_blog
    if self.blog_id
      blog = Blog.find(self.blog_id)
      blog.articles.each do |art|
        errors.add(:title, "There is one article with that title on the blog you chose") if (art.title == self.title && art.id != self.id)
      end
    end
  end
end
