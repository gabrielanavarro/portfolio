class Institution < ActiveRecord::Base
  has_many :jobs
  has_many :projects
  has_many :courses
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => ":style/institutions.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :description, presence: true
  validates :acronym, presence: true, uniqueness: { case_sensitive: false }
  validates :addresses_id, presence: true

end
