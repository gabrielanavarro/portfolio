class Project < ActiveRecord::Base
  has_many :users
  has_many :institutions

  validates :name, presence: true
  validates :institution_id, presence: true
  validates :user_id, presence: true
  validate :check_if_user_has_job_with_same_name
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => ":style/user.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  def check_if_user_has_job_with_same_name
    if self.user_id
      user = User.find(self.user_id)
      user.projects.each do |project|
        errors.add(:name, "User has a project with the same name already") if project.name == self.name
      end
    end
  end
end
