class Address < ActiveRecord::Base
  has_many :institutions
  validates :identifier, presence: true, uniqueness: true
  validates :country, presence: true
  validates :city, presence:true
  validates :state, presence: true
end
