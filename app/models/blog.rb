class Blog < ActiveRecord::Base
  has_many :articles

  validates :name, presence: true
  validates :description, presence: true
  validates :user_id, presence: true
  validate :check_name_of_the_users_blog

  has_attached_file :cover, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => ":style/blogs.png"
  validates_attachment_content_type :cover, :content_type => /\Aimage\/.*\Z/

  def check_name_of_the_users_blog
    if self.user_id
      user = User.find(self.user_id)
      user.blogs.each do |blog|
        errors.add(:name, "There is one blog with that name") if (blog.name == self.name && blog.id != self.id)
      end
    end
  end
end
