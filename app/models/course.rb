class Course < ActiveRecord::Base
  has_many :users
  has_many :institutions

  validates :name, presence: true
  validates :institution_id, presence: true
  validates :user_id, presence: true
end
