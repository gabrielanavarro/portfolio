class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :authenticate_user!
  protect_from_forgery with: :exception
  before_filter :environment_domain
  before_filter :blog_posts
  before_action :can_sign_up?, if: :devise_controller?
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale

  protected

  def set_locale
    I18n.locale = extract_locale_from_tld || I18n.default_locale
  end

  # Get locale from top-level domain or return nil if such locale is not available
  # You have to put something like:
  #   127.0.0.1 application.com
  #   127.0.0.1 application.it
  #   127.0.0.1 application.pl
  # in your /etc/hosts file to try this out locally
  def extract_locale_from_tld
    parsed_locale = request.host.split('.').last
    if parsed_locale == "br"
      parsed_locale = "pt-BR"
    end
    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end

  def blog_posts
    return if Rails.env.test?
    if user_signed_in?
      @blog_to_show = current_user.blogs.where(show_on_home_page: true)
    else
      @blog_to_show = Blog.where(show_on_home_page: true)
    end
  end

  def environment_domain
    @environment = Environment.find_by_name(Rails.configuration.env_name)
    if @environment.blank?
      Environment.destroy_all
      @environment = Environment.create(:name => Rails.configuration.env_name, :can_sign_up => true)
    end
    @environment
  end

  def can_sign_up?
    if controller_name == "registrations" && (action_name == "sign_up" || action_name == "new")
      if @environment.can_sign_up
        return true
      else
        @message = "This environment doesn't allow registrations"
        render :template => 'shared/_access_denied', :status => 403, :formats => [:html]
      end
    end
    return true
  end

  def configure_permitted_parameters
    params = [:name, :about_me, :avatar, :github, :gitlab, :country, :linkedin,
              :email, :password, :password_confirmation, :birthday, :remember_me,
              :current_password]
    devise_parameter_sanitizer.for(:sign_up){ |u| u.permit(params) }
    devise_parameter_sanitizer.for(:account_update){ |u| u.permit(params) }
  end
end
