class InstitutionsController < PrivateController
  skip_before_filter :authenticate_user!, only: [:show, :index]
  before_action :set_institution, only: [:show, :edit, :update, :destroy]
  before_action :set_address, only: [:edit, :new, :create, :update]

  # GET /institutions
  # GET /institutions.json
  def index
    @institutions = Institution.all
  end

  # GET /institutions/1
  # GET /institutions/1.json
  def show
  end

  # GET /institutions/new
  def new
    @institution = Institution.new
  end

  # GET /institutions/1/edit
  def edit
  end

  # POST /institutions
  # POST /institutions.json
  def create
    @institution = Institution.new(institution_params)

    respond_to do |format|
      save_object @institution, format
    end
  end

  # PATCH/PUT /institutions/1
  # PATCH/PUT /institutions/1.json
  def update
    respond_to do |format|
      update_params @institution, institution_params, format
    end
  end

  # DELETE /institutions/1
  # DELETE /institutions/1.json
  def destroy
    destroy_object @institution
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_institution
      @institution = Institution.find(params[:id])
    end

    def set_address
      @addresses = []
      Address.all.each{|address| @addresses << [address.identifier, address.id]}
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def institution_params
      params.require(:institution).permit(:name, :description, :avatar, :acronym, :addresses_id)
    end
end
