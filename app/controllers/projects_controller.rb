class ProjectsController < PrivateController
  skip_before_filter :authenticate_user!, only: [:show, :index]
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :set_institution, only: [:edit, :new, :create, :update]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)
    add_inst_and_user @project, project_params

    respond_to do |format|
      save_object @project, format
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      update_params @project, project_params, format
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    destroy_object @project
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    def set_institution
      @institutions = []
      Institution.all.each {|inst| @institutions << [inst.acronym, inst.id]}
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :institution_id, :responsabilities, :position, :start_date, :current, :end_date, :avatar)
    end
end
