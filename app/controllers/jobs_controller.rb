class JobsController < PrivateController
  skip_before_filter :authenticate_user!, only: [:show, :index]
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  before_action :set_institution, only: [:edit, :new, :create, :update]

  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = Job.all
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new

  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)
    add_inst_and_user @job, job_params

    respond_to do |format|
      save_object @job, format
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      update_params @job, job_params, format
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    destroy_object @job
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    def set_institution
      @institutions = []
      Institution.all.each {|inst| @institutions << [inst.acronym, inst.id]}
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:name, :institution_id, :responsabilities, :position, :start_date, :current, :end_date, :link, :avatar)
    end
end
