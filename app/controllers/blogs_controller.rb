class BlogsController < PrivateController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user!, only: [:show, :index]

  # GET /blogs
  # GET /blogs.json
  def index
    @blogs = Blog.order('name ASC')
    if request.xhr?
      set_new_home_page_blog
    end
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
  end

  # GET /blogs/new
  def new
    @blog = Blog.new
  end

  # GET /blogs/1/edit
  def edit
  end

  # POST /blogs
  # POST /blogs.json
  def create
    @blog = Blog.new(blog_params)
    @blog.user_id = current_user.id

    respond_to do |format|
      save_object @blog, format
    end
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      update_params @blog, blog_params, format
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    destroy_object @blog
  end

  def set_new_home_page_blog
    unless params[:value].nil?
      current_user.blogs.each do |blog|
        blog.show_on_home_page = false
        blog.save!
      end
      blog = Blog.find(params[:value])
      blog.show_on_home_page = true
      blog.save
    end

    render :index
  end

  private


    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      @blog = Blog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      params.require(:blog).permit(:name, :description)
    end
end
