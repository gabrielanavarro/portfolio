class ArticlesController < PrivateController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_blogs, only: [:new, :edit, :create, :update]
  skip_before_filter :authenticate_user!, only: [:show, :index]
  before_filter :is_logged_in?, only: [:edit, :update, :destroy, :new, :create]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
    @blogs = User.first.blogs
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    respond_to do |format|
      save_object @article, format
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      update_params @article, article_params, format
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    destroy_object @article
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    def set_blogs
      @blogs = []
      current_user.blogs.each {|blog| @blogs << [blog.name, blog.id]}
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :abstract, :body, :accept_comments, :published, :blog_id)
    end
end
