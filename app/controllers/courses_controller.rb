class CoursesController < PrivateController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :set_institution, only: [:edit, :new, :create, :update]
  skip_before_filter :authenticate_user!, only: [:show, :index]

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.all
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
  end

  # GET /courses/new
  def new
    @course = Course.new
  end

  # GET /courses/1/edit
  def edit
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)
    add_inst_and_user @course, course_params

    respond_to do |format|
      save_object @course, format
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      update_params @course, course_params, format
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    destroy_object @course
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_course
    @course = Course.find(params[:id])
  end

  def set_institution
    @institutions = []
    Institution.all.each {|inst| @institutions << [inst.acronym, inst.id]}
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def course_params
    params.require(:course).permit(:name, :start_date, :end_date, :institution_id, :gpa, :degree)
  end
end
