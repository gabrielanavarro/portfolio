class EnvironmentsController < PrivateController
  before_action :set_environment, only: [:show, :edit, :update, :destroy]
  before_filter :is_logged_in?, only: [:edit, :update, :destroy, :new, :create]

  # GET /environments
  # GET /environments.json
  def index
    @environments = Environment.all
  end

  # GET /environments/1
  # GET /environments/1.json
  def show
  end

  # GET /environments/new
  def new
    @environment = Environment.new
  end

  # GET /environments/1/edit
  def edit
  end

  # POST /environments
  # POST /environments.json
  def create
    @environment = Environment.new(environment_params)

    respond_to do |format|
      save_object @environment, format
    end
  end

  # PATCH/PUT /environments/1
  # PATCH/PUT /environments/1.json
  def update
    respond_to do |format|
      update_params @environment, environment_params, format
    end
  end

  # DELETE /environments/1
  # DELETE /environments/1.json
  def destroy
    destroy_object @environment
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_environment
      @environment = Environment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def environment_params
      params.require(:environment).permit(:name, :can_sign_up)
    end
end
