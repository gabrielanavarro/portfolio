class PrivateController < ApplicationController
  private

  def is_logged_in?
    unless user_signed_in?
      access_denied
    end
    true
  end

  def access_denied
    @message = "You need to be logged in to do this."

    render :template => 'admin/_access_denied', :status => 403, :formats => [:html]
  end

  def add_inst_and_user object, params
    object.institution_id = params[:institution_id].to_i
    object.user_id = current_user.id
  end

  def save_object object, format
    if object.save
      success_update_or_save object, format
    else
      error_update_or_save object, format, :new
    end
  end

  def update_params object, params, format
    if object.update(params)
      success_update_or_save object, format
    else
      error_update_or_save object, format, :edit
    end
  end

  def success_update_or_save object, format
    format.html { redirect_to object, notice: "#{object.class.name} was successfully created." }
    format.json { render :show, status: :created, location: object }
  end

  def error_update_or_save object, format, redir
    format.html { render redir }
    format.json { render json: object.errors, status: :unprocessable_entity }
  end

  def destroy_object object
    object.destroy
    respond_to do |format|
      format.html { redirect_to object, notice: "#{object.class.name} was successfully destroyed." }
      format.json { head :no_content }
    end
  end
end
