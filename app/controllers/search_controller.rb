class SearchController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]

  def index
    @results = {}
    institutions = []
    jobs = []
    projects = []
    articles = []
    blogs = []
    courses = []
    term = "%" + params[:search].to_s + "%"

    institutions = Institution.where('lower(description) like ? OR lower(name) like ?', term.downcase, term.downcase)
    jobs = Job.where('lower(responsabilities) like ? OR lower(name) like ? OR lower(position) like ?', term.downcase, term.downcase, term.downcase)
    projects = Project.where('lower(responsabilities) like ? OR lower(name) like ? OR lower(position) like ?', term.downcase, term.downcase, term.downcase)
    articles = Article.where('lower(title) like ? OR lower(body) like ? OR lower(abstract) like ?', term.downcase, term.downcase, term.downcase)
    blogs = Blog.where('lower(name) like ? OR lower(description) like ?', term.downcase, term.downcase)
    courses = Course.where('lower(name) like ? OR lower(degree) like ?', term.downcase, term.downcase)

    @results = {"institutions" => institutions, "jobs" => jobs, "blogs" => blogs,
                "projects" => projects, "articles" => articles, "courses" => courses}
  end
end
