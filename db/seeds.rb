# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Environment.create(:name => Rails.configuration.env_name, :can_sign_up => true)

user = User.create(:name => "Gabriela Navarro", :email => "navarro1703@gmail.com", :password => "12345678", :password_confirmation => "12345678")
blog = Blog.create(:name => "Tech up", :description => "This is a blog for my user", :user_id => user.id, show_on_home_page: true)
article1 = Article.create(:title => "First post", :abstract => "This is an abstract", :body => "This is a body", :blog_id => blog.id, :user_id => user.id)
article2 = Article.create(:title => "Second post", :abstract => "This is an abstract", :body => "This is a body", :blog_id => blog.id, :user_id => user.id)
article3 = Article.create(:title => "Third post", :abstract => "This is an abstract", :body => "This is a body", :blog_id => blog.id, :user_id => user.id)
article4 = Article.create(:title => "Fourth post", :abstract => "This is an abstract", :body => "This is a body", :blog_id => blog.id, :user_id => user.id)

fairfield = Address.create(country: "USA", city: "Farfield", state: "Connecticut", identifier: "Fairfield - CT")
boston = Address.create(country: "USA", city: "Boston", state: "Massachussets", identifier: "Boston - MA")
gama = Address.create(country: "Brazil", city: "Gama", state: "Distrito Federal", identifier: "Gama - DF")

# Jobs
Institution.create(:name => "Laboratório Avançado de Produção, Pesquisa & Inovação em Software", :description => "O Laboratório Avançado de Produção, Pesquisa & Inovação em Software (LAPPIS), localizado na Faculdade UnB Gama (FGA) foi criado em 2012 e foi concebido para atuar em áreas tecnológicas desde sistemas de informação até os sistemas embarcados, em especial, objetivando as oportunidades de pesquisas teóricas e aplicadas.O LAPPIS tem como objetivo contribuir com o desenvolvimento de projetos de software ao passo que complementamos a formação de Engenheiros de Software (capazes de lidar com problemas ao pensar em soluções computacionais e implamentá-las efetivamente), por meio de Métodos Ágeis, Software
Livre, Segurança e Trabalho colaborativo centrado nas pessoas.Com isso, oferece aos alunos de graduação em Engenharia de Software a oportunidade de aplicar os conceitos e as tecnologias em um ambiente produção de software real, sob orientação de professores especialistas nas áreas dos projetos do laboratório.", :acronym => "LAPPIS", :addresses_id => gama.id)

# Universities
Institution.create(:name => "Universidade de Brasilia", :description => "A Universidade de Brasília foi inaugurada em 21 de abril de 1962. Atualmente, possui 2.445 professores, 2.630 técnicos-administrativos e 28.570 alunos regulares e 6.304 de pós-graduação. É constituída por 26 institutos e faculdades e 21 centros de pesquisa especializados.
Oferece 109 cursos de graduação, sendo 31 noturnos e 10 a distância. Há ainda 147 cursos de pós-graduação stricto sensu e 22 especializações lato sensu. Os cursos estão divididos em quatro campi espalhados pelo Distrito Federal: Darcy Ribeiro (Plano Piloto), Planaltina, Ceilândia e Gama. Os órgãos de apoio incluem o Hospital Universitário, a Biblioteca Central, o Hospital Veterinário e a Fazenda Água Limpa.", :acronym => "UnB", :addresses_id => gama.id)

Institution.create(:name => "Fairfield University", :description => "The moment you arrive at Fairfield University everything feels different. Our 200-acre idyllic campus is situated on Connecticut's coastline, providing for beautiful, unforgettable surroundings. But it's more than that - it's our dedication to each and every student's personal growth that makes our environment so unique.
We've been graduating adept, accomplished students since being founded by the Society of Jesus (the Jesuits) over 70 years ago. We're committed to fostering a strong sense of community, where the learning goes deeper, the friendships are more intense, and the experiences stay with you long after graduation. Our 5,000 undergraduate and graduate students will become your friends and support system, and our 41 majors, 16 interdisciplinary minors and 40 graduate programs will give you the opportunity to turn your interests into passions and careers.", :acronym => "FU", :addresses_id => fairfield.id)

Institution.create(:name => "Boston University", :description => "Boston University is anything but insular. We look beyond our classrooms and borders. You’ll find our students and faculty in more than 75 countries, on seven continents, including Antarctica, pursuing research, study, and service. Back on campus, stroll up and down Comm. Ave. and you’ll hear countless languages and dialects. Our professors, students, and researchers hail from more than 140 countries. It should come as no surprise that BU launched the nation’s first collegiate international exchange program more than a century ago. Engaging with people, ideas, and pressing issues that impact the world has always been vital to the University’s mission. Simply put, global engagement is in our DNA.", :acronym => "BU", :addresses_id => boston.id)
