class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :body
      t.text :abstract
      t.integer :parent_id
      t.boolean :accept_comments
      t.boolean :published
      t.belongs_to :user, index: true
      t.timestamps null: false
    end
  end
end
