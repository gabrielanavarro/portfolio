class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :name
      t.string :responsabilities
      t.string :position
      t.string :link
      t.date :start_date
      t.date :end_date
      t.belongs_to :user, index:true
      t.belongs_to :institution, index:true
      t.boolean :current
      t.timestamps null: false
    end
  end
end
