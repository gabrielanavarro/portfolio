class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.integer :gpa
      t.string :degree
      t.boolean :current
      t.belongs_to :institution, index: true
      t.belongs_to :user, index: true
      t.timestamps null: false
    end
  end
end
