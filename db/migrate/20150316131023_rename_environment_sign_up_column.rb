class RenameEnvironmentSignUpColumn < ActiveRecord::Migration
  def change
    rename_column :environments, :can_sign_up?, :can_sign_up
  end
end
