class AddFieldsToBlog < ActiveRecord::Migration
  def change
    change_table :blogs do |t|
      t.text :description
      t.attachment :cover
      t.boolean :show_on_home_page
    end
  end
end
