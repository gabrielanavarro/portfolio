class AddAddressAndAcronymToInstitutions < ActiveRecord::Migration
  def change
    change_table :institutions do |t|
      t.string :acronym
      t.belongs_to :addresses
    end
  end
end
