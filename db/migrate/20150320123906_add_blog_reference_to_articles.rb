class AddBlogReferenceToArticles < ActiveRecord::Migration
  def change
    change_table :articles do |t|
      t.belongs_to :blog, index: true
    end
  end
end
