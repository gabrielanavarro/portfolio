class AddFieldsToUserTable < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :github
      t.string :gitlab
      t.string :linkedin
      t.string :about_me
      t.string :country
      t.string :name
      t.date :birthday
    end
  end
end
