class AddIdentifierToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :identifier, :string
  end
end
