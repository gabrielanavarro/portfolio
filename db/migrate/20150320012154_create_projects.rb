class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :responsabilities
      t.string :position
      t.date :start_date
      t.date :end_date
      t.boolean :current
      t.belongs_to :user, index: true
      t.belongs_to :institution, index: true
      t.timestamps null: false
    end
  end
end
