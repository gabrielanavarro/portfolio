class AddImagesToInstitutions < ActiveRecord::Migration
  def change
    add_attachment :institutions, :avatar
  end
end
